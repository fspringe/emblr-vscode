# emblR 23-11-21: VSCode as R Editor

## Why it is useful:
- Highly customizable editor with almost unlimited extensions, keybindings, shortcuts, etc.
- or R: Capability to run multiple R sessions simultaneously and open multiple windows of the same script, which is great for data analysis tasks.
- Remote development support: Very easy setup of stable SSH connections to remote servers.
- Multi-language support.

## Why it is (sometimes) a pain: 
- Takes some time to set up and configure properly
- Code completion/suggestions and workspace overview are not as robust as in RStudio.
- Definitely not an "out of the box" solution; requires some initial customization.

## Setup R in VSCode

### Setup R
It is highly recommended to manage R and all libraries using ```conda```. 
1) Create conda environment: ```conda create -n r_env``` and activate.
2) [For ARM-Mac users: set environment to consider only osx-86 packages:
```conda config --env --set subdir osx-64```]
3) Download R: ```mamba install -c conda-forge r-base=4.3.1```
4) Pin r-base version: ```echo "r_base=4.3.1" > <PathToCondaEnv>/conda-meta/pinned```
5) Install following packages to get started: ```mamba install -c conda-forge r-languageserver radian r-httpgd```

### Setup VS Code
1) Install ```R``` extension
2) Turn on R Session watcher and add this to your ```~/.Rprofile```
```
if (interactive() && Sys.getenv("RSTUDIO") == "") {
  source(file.path(Sys.getenv(if (.Platform$OS.type == "windows") "USERPROFILE" else "HOME"), ".vscode-R", "init.R"))
}
```
3) Suggested extensions: ```Path Autocomplete```, ```Better Comments```, ```Color Highlight```

## Useful links

[How to not get lost in VSCode-R](https://statnmap.com/2021-10-09-how-not-to-be-lost-with-vscode-when-coming-from-rstudio/)

[RSessionWatcher](https://github.com/REditorSupport/vscode-R/wiki/R-Session-watcher#advanced-usage-for-self-managed-r-sessions)

[Suggested Settings](https://renkun.me/2020/04/14/writing-r-in-vscode-working-with-multiple-r-sessions/)





